@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Registros</h1>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>APELLIDO PATERNO</th>
                    <th>APELLIDO MATERNO</th>
                    <th>FECHA NACIMIENTO</th>
                </tr>
                @foreach ($registro as $reg)
                    <tr>
                        <td>{{$reg->id}}</td>
                        <td>{{$reg->nombre}}</td>
                        <td>{{$reg->apellidopaterno}}</td>
                        <td>{{$reg->apellidomaterno}}</td>
                        <td>{{$reg->fechadenacimiento}}</td>
                    </tr>

                @endforeach
                </thead>
            </table>
            
        </div>
        <div>  
        <br><br><br>
            <form action="{{route('registros.create')}}" method="GET">
                <input type="submit" value="Nuevo" class="btn btn-success">

            </form> 
        </div>
        
    </div>
    
</div>
@endsection