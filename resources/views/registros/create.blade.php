@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{route('registros.store')}}" method="POST">
@csrf
    <div class="row justify-content-center form-group">
        <div class="col-md-8">

        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" required>  
        </div>
        <div class="row form-group">
            <div class="form-group col-md-6">
                <label for="apellidopaterno">Apellido Paterno</label>
                <input type="text" class="form-control" id="apellidopaterno" name="apellidopaterno" required>  
            </div>
            <div class="form-group col-md-6">
                <label for="apellidomaterno">Apellido Materno</label>
                <input type="text" class="form-control" id="apellidomaterno" name="apellidomaterno" required>  
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="fechadenacimiento">Fecha de Nacimiento</label>
            <input class="form-control" id="fechadenacimiento" name="fechadenacimiento" placeholder="Año/Mes/Dia" type="text" required>
        </div>  
        <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
        </div>
    </form>
</div>
@endsection